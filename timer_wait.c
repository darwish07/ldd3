/*
 * Create a sysfs attribute.
 *
 * This attribute prints time after sleeping for one second.
 * It sleeps using different methods
 *
 * For the sysfs interface, check:
 *
 *     https://www.kernel.org/doc/Documentation/filesystems/sysfs.txt
 */

#include <linux/init.h>			/* __init, __exit */
#include <linux/module.h>		/* module_init(), module_exit() */

#include <linux/completion.h>		/* completions */
#include <linux/device.h>		/* device_create(), class_create() */
#include <linux/jiffies.h>		/* jiffies, time_before(), time_after() */
#include <linux/kernel.h>		/* Basics: abs(), etc. */
#include <linux/sched.h>		/* schedule() and friends */
#include <linux/wait.h>			/* wait_queues */
#include <linux/stat.h>			/* S_IRUGO (0444) */
#include <linux/sysfs.h>		/* sysfs_create_file() */
#include <linux/timer.h>		/* scheduling atomic timer handlers*/
#include <linux/timekeeping.h>		/* getnstimeofday() */

#define TIMER_WAIT_DEVICE_NAME	   "timer_wait_dev"
#define TIMER_WAIT_CLASS_NAME	   "timer_wait"
#define TIMER_WAIT_SYSFS_FILE_NAME "timer-wait"

#define TIMER_WAIT_DELAY_SECONDS    1
#define TIMER_WAIT_DELAY_JIFFIES   (HZ * TIMER_WAIT_DELAY_SECONDS)

static struct class *timer_wait_class;
static struct device *timer_wait_device;

enum SILENT_WAIT_MODE {
	SILENT_WAIT_BUSYLOOP = 0,
	SILENT_WAIT_BUSY_SCHED,
	SILENT_WAIT_WAITQUEUE_TIMEOUT,
	SILENT_WAIT_SCHEDULE_TIMEOUT,
	SILENT_WAIT_TIMER,
	SILENT_WAIT_MAX,
};

static enum SILENT_WAIT_MODE get_wait_mode(void) {
	static enum SILENT_WAIT_MODE mode = -1;

	mode = (mode + 1) % SILENT_WAIT_MAX;
	return mode;
}

/*
 * Timer hander. Always runs in atomic context (no sleep, `current'
 * meaningless, etc.)
 */
static void timer_fn(unsigned long arg) {
	struct completion *completion;

	completion = (struct completion *)arg;
	complete(completion);
}

/*
 * Sysfs show() method rules [Documentation/filesystems/sysfs.txt]:
 *
 * - Given buffer will always be PAGE_SIZE bytes
 * - Must return total size of bytes printed into given buffer
 * - Can return normal kernel errno errors
 */
static ssize_t timer_wait_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf) {
	enum SILENT_WAIT_MODE mode;
	struct timespec now1, now2;
	unsigned long j1, j2;
	wait_queue_head_t wait_queue;
	struct completion completion;
	struct timer_list timer;
	unsigned long jtarget;
	ssize_t ret = 0;

	mode = get_wait_mode();
	jtarget = jiffies + TIMER_WAIT_DELAY_JIFFIES;

	getnstimeofday(&now1);
	j1 = jiffies;

	switch (mode) {
	case SILENT_WAIT_BUSYLOOP:
		/*
		 * While waiting: process always busy-loops in kernel mode, in R state
		 *
		 * If interrupts disabled: this CPU core goes bye-bye! Jiffies not updated
		 * by the timer interrupt handler, and an infinite loop
		 *
		 * If kernel not pre-emptible: CPU gets stuck in this loop. Otherwise the
		 * scheduler can pre-empt this kernel code path
		 */
		ret += sprintf(buf, "Using a dumb cpu_relax() busy loop:");
		while (time_before(jiffies, jtarget))
			cpu_relax();
		break;

	case SILENT_WAIT_BUSY_SCHED:
		/*
		 * While waiting: process always stays "in the runqueue", also R state
		 *
		 * This will not needlessly consume CPU cycles if there are other
		 * processes in the runqueue. If there are not any, then turns into a
		 * tight busy loop (scheduler runs this kernel code path, returning to
		 * the scheduler, returning to this code path, ad infinitum). This also
		 * means sabotaging the idle task work and CPU power savings work
		 */
		ret += sprintf(buf, "Using a dumb schedule() busy loop:");
		while (time_before(jiffies, jtarget))
			schedule();
		break;

	case SILENT_WAIT_WAITQUEUE_TIMEOUT:
		/*
		 * Just for illustration. Better use schedule_timeout() in this case.
		 */
		ret += sprintf(buf, "Using a wait_event_interruptible_timeout():");
		init_waitqueue_head(&wait_queue);
		wait_event_interruptible_timeout(wait_queue, 0, TIMER_WAIT_DELAY_JIFFIES);
		break;

	case SILENT_WAIT_SCHEDULE_TIMEOUT:
		ret += sprintf(buf, "Using a basic schedule_timeout():");
		set_current_state(TASK_INTERRUPTIBLE);
		schedule_timeout(TIMER_WAIT_DELAY_JIFFIES);
		break;

	case SILENT_WAIT_TIMER:
		ret += sprintf(buf, "Using a programmed timer and wait_event():");
		init_completion(&completion);
		init_timer(&timer);
		
		timer.data = (unsigned long)&completion;
		timer.function = timer_fn;
		timer.expires = jtarget;
		add_timer(&timer);

		if (wait_for_completion_interruptible(&completion))
			return -ERESTARTSYS;
		break;

	default:
		dev_notice(timer_wait_device, "Unknown wait mode = %d. Not sleeping!", mode);
	}

	getnstimeofday(&now2);
	j2 = jiffies;

	ret += sprintf(buf + ret, "\n\n");
	ret += sprintf(buf + ret, "Seconds before wait = %lu\n", now1.tv_sec);
	ret += sprintf(buf + ret, "Seconds after wait = %lu\n", now2.tv_sec);
	ret += sprintf(buf + ret, "\n\n");
	ret += sprintf(buf + ret, "Wait period: %lu seconds\n", now2.tv_sec - now1.tv_sec);
	ret += sprintf(buf + ret, "Wait period: %lu jiffies\n", j2 - j1);
	return ret;
}

static struct kobj_attribute timer_test_attr =
	__ATTR(timer-wait, S_IRUGO, timer_wait_show, NULL);

static int __init time_init(void) {
	int ret;

	timer_wait_class = class_create(THIS_MODULE, TIMER_WAIT_CLASS_NAME);
	if (IS_ERR(timer_wait_class)) {
		ret = PTR_ERR(timer_wait_class);
		goto out1;
	}

	timer_wait_device = device_create(timer_wait_class, NULL, 0, NULL, TIMER_WAIT_DEVICE_NAME);
	if (IS_ERR(timer_wait_device)) {
		ret = PTR_ERR(timer_wait_device);
		goto out2;
	}

	ret = sysfs_create_file(&timer_wait_device->kobj, &timer_test_attr.attr);
	if (ret) {
		dev_err(timer_wait_device, "Couldn't create sysfs file: %s", TIMER_WAIT_SYSFS_FILE_NAME);
		goto out3;
	}

	return 0;

out3:	device_unregister(timer_wait_device);
out2:	class_destroy(timer_wait_class);
out1:	return ret;
}

static void __exit time_exit(void) {
	sysfs_remove_file(&timer_wait_device->kobj, &timer_test_attr.attr);

	device_unregister(timer_wait_device);
	class_destroy(timer_wait_class);
}

module_init(time_init);
module_exit(time_exit);
MODULE_LICENSE("GPL");
