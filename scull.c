#include <linux/init.h>
#include <linux/module.h>

#include <linux/cdev.h>			/* struct cdev */
#include <linux/fs.h>			/* alloc_chrdev_region(), inodes, etc. */
#include <linux/list.h>			/* linked list */
#include <linux/mutex.h>		/* Better than semaphores! (Documentation/locking/mutex-design.txt) */
#include <linux/slab.h>			/* kmalloc(), kfree(), etc. */

#include <linux/uaccess.h>		/* copy_to/from_user() */

struct scull_qset;			/* quantum set: An array of quantums */
#define SCULL_QSET_QUANTUMS_COUNT 1000	/* Number of 'quantums' in a qset */
#define SCULL_QUANTUM_SIZE 4000		/* Length for each quantum */
#define SCULL_QSET_TOTAL_SIZE		(SCULL_QSET_QUANTUMS_COUNT * SCULL_QUANTUM_SIZE)

static struct scull_dev {
	struct cdev cdev;
	// TODO: Remove this 'special' qset pointer, put the
	// scull_qsets_list HEAD here
	struct scull_qset *qset;	/* Pointer to first quantum set */
	loff_t size;			/* Amount of data stored here */
	struct mutex mutex;		/* Protection/blocking for read(), write() */
} scull_dev;

static int scull_nr_devs = 1;
static int scull_minor = 0;
static dev_t scull_devid = 0;

/*
 *                           MEMORY MANAGEMENT
 *
 * SCULL memory is organized as a linked-list of Quantum Sets (Qsets).
 *
 * Each Qset contains an array of Quantums. Each quantum is a unique,
 * singly-allocated kmalloc-ed area. So the quantums in a qset aren't
 * contiguous; they're individually-allocated by separate kmallocs.
 */

// TODO: Put this in 'struct scull_dev'
static LIST_HEAD(scull_qsets_list);
struct scull_qset {
	void *quantums[SCULL_QSET_QUANTUMS_COUNT];
	struct list_head list_node;
};

/* Translate a flat file position offset (typically from read and
 * write system calls) to offsets within the SCULL memory hierarchy. */
static void file_pos_to_scull_offsets(loff_t f_pos, loff_t *ret_qset_idx, loff_t *ret_quantum_idx, loff_t *ret_quantum_offset) {
	loff_t qset_offset;

	/* Within the whole character device memory hierarchy */
	*ret_qset_idx = f_pos / SCULL_QSET_TOTAL_SIZE;
	qset_offset = f_pos % SCULL_QSET_TOTAL_SIZE;

	/* Within the quantum-set identified above */
	*ret_quantum_idx = qset_offset / SCULL_QUANTUM_SIZE;
	*ret_quantum_offset = qset_offset % SCULL_QUANTUM_SIZE;
}

static struct scull_qset *qset_alloc_init(void) {
	struct scull_qset *qset;
	int i;

	qset = kzalloc(sizeof(struct scull_qset), GFP_KERNEL);
	if (!qset)
		return NULL;

	for (i = 0; i < SCULL_QSET_QUANTUMS_COUNT; i++) {
		qset->quantums[i] = kzalloc(SCULL_QUANTUM_SIZE, GFP_KERNEL);
		if (!qset->quantums[i])
			return NULL;
	}

	INIT_LIST_HEAD(&qset->list_node);
	return qset;
}

static void qset_free(struct scull_qset *qset) {
	int i;

	if (!qset)
		return;

	for (i = 0; i < SCULL_QSET_QUANTUMS_COUNT; i++)
		kfree(qset->quantums[i]);

	list_del(&qset->list_node);
	kfree(qset);
}

static void scull_trim(struct scull_dev *dev) {
	struct scull_qset *qset, *tmp;

	if (!dev || !dev->qset)
		return;

	list_for_each_entry_safe(qset, tmp, &scull_qsets_list, list_node)
		qset_free(qset);

	dev->qset = NULL;
	dev->size = 0;
}

static struct scull_qset *get_qset_by_idx(struct scull_dev *dev, unsigned idx) {
	struct list_head *pos;
	struct scull_qset *qset;
	unsigned i = 0;

	if (!dev->qset)
		return NULL;

	list_for_each(pos, &scull_qsets_list) {
		if (i == idx) {
			qset = container_of(pos, struct scull_qset, list_node);
			return qset;
		}
		++i;
	}

	return NULL;
}

/*
 *                            FILE OPERATIONS
 *
 * The inode structure is used by the kernel internally to represent
 * files. Therefore, it is different from __the file structure__ that
 * represents an open file descriptor.
 */

static int scull_open(struct inode *inode, struct file *file) {
	struct scull_dev *dev;
	struct cdev *cdev;

	/* Extract global device structure and save it, in the open
	 * file descriptor 'private_data' field, so it is readily
	 * available for other file operations (read, write, etc.) */
	cdev = inode->i_cdev;
	dev = container_of(cdev, struct scull_dev, cdev);
	file->private_data = dev;

	if ((file->f_flags & O_ACCMODE) == O_WRONLY)
		scull_trim(dev);

	return 0;
}

/* Kernel keeps a reference count on the opened file descriptor.
 * Naturally fork() or dup() increases the refcount. Below release()
 * method is _only_ called with the refcount reaches 0.
 *
 * This relationship guarantees that the driver sees only one
 * release() for each call to open() */
static int scull_release(struct inode *inode, struct file *file) {
	return 0;
}

static ssize_t scull_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos) {
	struct scull_dev *dev;
	struct scull_qset *qset;
	loff_t qset_idx, quantum_idx, quantum_offset;
	int ret;

	dev = file->private_data;
	ret = 0;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	if (*f_pos >= dev->size)
		goto out;
	if (*f_pos + count >= dev->size)
		count = dev->size - *f_pos;

	file_pos_to_scull_offsets(*f_pos, &qset_idx, &quantum_idx, &quantum_offset);

	qset = get_qset_by_idx(dev, qset_idx);
	if (!qset)
		goto out;
	if (!qset->quantums[quantum_idx])
		goto out;

	/* Simplify: read only up to the end of this quantum.
	 * Let user-space re-call us for the rest! */
	count = min(count, (size_t)(SCULL_QUANTUM_SIZE - quantum_offset));

	ret = copy_to_user(buf, ((uint8_t *)qset->quantums[quantum_idx]) + quantum_offset, count);
	if (ret) {
		ret = -EINVAL;
		goto out;
	}

	*f_pos += count;
	ret = count;
out:
	mutex_unlock(&dev->mutex);
	return ret;
}

static ssize_t scull_write(struct file *file, const char __user *buf, size_t count, loff_t *f_pos) {
	struct scull_dev *dev;
	struct list_head *pos;
	struct scull_qset *qset, *tail_qset = NULL;
	loff_t qset_idx, quantum_idx, quantum_offset;
	int i, ret;

	dev = file->private_data;
	ret = 0;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	file_pos_to_scull_offsets(*f_pos, &qset_idx, &quantum_idx, &quantum_offset);

	// TODO: Streamline below 3 chunks into one loop by making
	// dev->qset a non-special case (putting only scull_qsets_list
	// HEAD in scull_dev instead). Then some if conditions in a
	// single body loop

	if (!dev->qset) {
		dev->qset = qset_alloc_init();		// TODO: Handle ENOMEM
		list_add_tail(&dev->qset->list_node, &scull_qsets_list);
	}

	i = 0;
	list_for_each(pos, &scull_qsets_list) {
		tail_qset = container_of(pos, struct scull_qset, list_node);
		if (i == qset_idx)
			break;
		++i;
	}

	for (; i < qset_idx; i++) {
		qset = qset_alloc_init();		// TODO: Handle ENOMEM
		list_add_tail(&qset->list_node, &tail_qset->list_node);
		tail_qset = qset;
	}

	/* Simplify: Write only up to end of quantum */
	count = min(count, (size_t)(SCULL_QUANTUM_SIZE - quantum_offset));

	ret = copy_from_user((uint8_t *)tail_qset->quantums[quantum_idx] + quantum_offset, buf, count);
	if (ret) {
		ret = -EFAULT;
		goto out;
	}

	*f_pos += count;
	dev->size = max(dev->size, *f_pos);
	ret = count;
out:
	mutex_unlock(&dev->mutex);
	return ret;
}

static struct file_operations scull_fops = {
	.owner = THIS_MODULE,
	.open = scull_open,
	.release = scull_release,
	.read = scull_read,
	.write = scull_write,
	/* If your driver does not supply methods to handle the vector
	 * operations, readv and writev are implemented with multiple
	 * calls to your read and write methods. In many situations,
	 * however, greater efficiency is acheived by implementing
	 * readv and writev directly. */
};

static int __init scull_init(void) {
	int r;

	r = alloc_chrdev_region(&scull_devid, scull_minor, scull_nr_devs, "scull");
	if (r < 0) {
		printk(KERN_ERR "scull: can't allocate a character device major number\n");
		return r;
	}

	cdev_init(&scull_dev.cdev, &scull_fops);
	mutex_init(&scull_dev.mutex);

	/* NOTE: As soon as cdev_add() below succeeds, the device
	 * is "live" and its operations can be called by the kernel */

	r = cdev_add(&scull_dev.cdev, scull_devid, scull_nr_devs);
	if (r < 0) {
		printk(KERN_ERR "scull: couldn't register character device\n");
		goto err1;
	}

	return 0;
err1:
	unregister_chrdev_region(scull_devid, scull_nr_devs);
	return r;
}

static void __exit scull_exit(void) {
	scull_trim(&scull_dev);

	cdev_del(&scull_dev.cdev);
	unregister_chrdev_region(scull_devid, scull_nr_devs);
}

module_init(scull_init);
module_exit(scull_exit);
MODULE_LICENSE("GPL");
