#include <linux/init.h>			/* module_init, module_exit, .. */
#include <linux/module.h>		/* MODULE_LICENSE */
#include <linux/sched.h>		/* current */
#include <generated/utsrelease.h>	/* UTS_RELEASE */

static char *whoami = "World";
static int howmany = 1;
module_param(whoami, charp, S_IRUGO);
module_param(howmany, int, S_IRUGO);

static int __init hello_init(void)
{
	int i;

	printk(KERN_INFO "\n----Module start----");
	printk(KERN_INFO "The process is '%s' (pid %i)\n", current->comm, current->pid);
	printk(KERN_INFO "This module was compiled against version: %s\n", UTS_RELEASE);

	for (i = 0; i < howmany; i++)
		printk(KERN_INFO "%d: Hello %s!\n", i, whoami);

	return 0;
}

static void __exit hello_exit(void)
{
	printk(KERN_INFO "Goodbye, beautiful world!\n");
	printk(KERN_INFO "The process was '%s' (pid %i)\n", current->comm, current->pid);
	printk(KERN_INFO "----Module end----\n");
}

module_init(hello_init);
module_exit(hello_exit);
MODULE_LICENSE("GPL");
