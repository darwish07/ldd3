#!/bin/bash

set -o xtrace
set -o errexit
set -o pipefail

function init() {
    mkdir -p devs
    mkdir -p mods
}

function build() {
    rm -f mods/*
    make
    mv *.ko mods
}

function clear_artifacts() {
    rm -f *.mod.c *.mod.o *.order *.symvers *.o *~
}

function run_hello() {
    sudo rmmod hello 2>/dev/null || true
    sudo insmod ./mods/hello.ko whoami=World howmany=1
}

function run_scull() {
    #
    # s_pipe and rest uses class_create() and device_create(),
    # thus making udev create everything automatically for us.
    # Nonetheless, we don't use this because it's very easy to
    # write to /dev/sda rather than /dev/s_pipe by mistake!
    #
    # For verification, do:
    #
    #   $ export CLASS=scull
    #   $ export DEVICE=s_pipe
    #   $ cat /sys/devices/virtual/$CLASS/$DEVICE/uevent
    #
    # Nodes will be automatically created. Like /dev/s_pipe
    # etc.
    #

    for module in scull completion s_pipe timer_wait; do
	sudo rmmod $module 2>/dev/null || true
	sudo insmod ./mods/$module.ko $*
    done

    for module in scull completion s_pipe; do
	devnode=${module}-dev
	rm -f devs/${devnode}[0-3]

	major=$(cat /proc/devices | grep $module | cut -d' ' -f1)
	if [[ ! -z "$major" ]]; then
	    for minor in 0; do
		node=devs/${devnode}${minor}
		sudo mknod $node c $major $minor
		sudo chown 1000:1000 $node
	    done
	fi
    done
}

init
build
clear_artifacts
run_hello
run_scull
