#include <linux/init.h>
#include <linux/module.h>

#include <linux/printk.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/completion.h>

static struct cdev comp_cdev;
static dev_t comp_devid;
static DECLARE_COMPLETION(rw_completion);

static ssize_t comp_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos) {
	printk(KERN_INFO "completion_test: process %i (%s) read(): going to sleep\n",
	       current->pid, current->comm);

	if (wait_for_completion_interruptible(&rw_completion))
		return -ERESTARTSYS;

	printk(KERN_INFO "completion_test: awoken process %i (%s)\n", current->pid, current->comm);
	return 0;
}

static ssize_t comp_write(struct file *file, const char __user *buf, size_t count, loff_t *f_pos) {
	printk(KERN_INFO "completion_test: process %i (%s) write(): awokening readers\n",
	       current->pid, current->comm);

	/* This wakes up only _one_ thread. Use complete_all() if
	 * you want the classic thundering-hurd CS problem ;-) */
	complete(&rw_completion);

	return count;
}

struct file_operations comp_fops = {
	.owner = THIS_MODULE,
	.read = comp_read,
	.write = comp_write,
};

static int __init comp_module_init(void) {
	int r;

	cdev_init(&comp_cdev, &comp_fops);

	r = alloc_chrdev_region(&comp_devid, 0, 1, "completion-testing");
	if (r < 0) {
		printk(KERN_ERR "comp-testing: Could not allocate cdev major/minor\n");
		return r;
	}

	r = cdev_add(&comp_cdev, comp_devid, 1);
	if (r < 0) {
		printk(KERN_ERR "Could not register character device\n");
		unregister_chrdev_region(comp_devid, 1);
		return r;
	}

	return 0;
}

static void __exit comp_module_exit(void) {
	cdev_del(&comp_cdev);
	unregister_chrdev_region(comp_devid, 1);
}

module_init(comp_module_init);
module_exit(comp_module_exit);
MODULE_LICENSE("GPL");
