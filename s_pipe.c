#include <linux/init.h>			/* __init, __exit, etc. */
#include <linux/module.h>		/* module_init(), module_exit(), etc. */

#include <linux/device.h>		/* struct device, devm_****() */
#include <linux/fs.h>			/* register_chrdev(), inodes, etc. */
#include <linux/list.h>			/* linked list */
#include <linux/mutex.h>		/* Better than semaphores! (Documentation/locking/mutex-design.txt) */
#include <linux/platform_device.h>	/* struct platform_device, struct platform_driver, etc. */
#include <linux/poll.h>                 /* poll_table_struct, etc. */
#include <linux/slab.h>			/* kmalloc(), kfree(), etc. */
#include <linux/uaccess.h>		/* copy_to/from_user() */

/*
 * We are a generic, pure-memory, driver, so let's register ourselves
 * under the "mem" class. Looking at /sys/class/mem, this is where
 * similar devices to ours are hosted: "mem", "null", "port", "random",
 * "urandom", "zero", etc.
 *
 * Originally, I wished to attach this device to the "mem" class, but
 * the "class_find()" method is not yet merged upstream! Thus I cannot
 * get a reference to that existing class. Create our own class instead.
 *
 * After loading this module, check below commands for more details:
 *
 *     $ ls /sys/class/mem
 *     $ ls /sys/class/scull
 *     $ ls -l /sys/class/scull/s_pipe
 *     $ cat /sys/devices/virtual/scull/s_pipe
 *     $ ls -l /dev/s_pipe
 */

#define SCULL_P_DEVICE_NAME	"s_pipe"
#define SCULL_P_CLASS_GROUP	"scull"

static dev_t scull_p_devid;
static struct class *scull_p_class;
static struct device *scull_p_device;

/*
 *                  SCULL PIPE: A Waitqueue Example
 *
 * A pipe-like, circular buffer, device. Writers, waiting for the full
 * buffer to be empty, are woken up after another process reads. Readers,
 * waiting for the empty buffer to be filled, are woken up when a writer
 * appears.
 */

#define SCULL_P_BUFFERSIZE	4096

static struct scull_pipe {
	char *buffer, *end;		/* beginning of buf, end of buf */
	char *readp, *writep;		/* read and write pointers, where to read, where to write! */
	size_t freespace;

	wait_queue_head_t readersq, writersq;	/* read and write queues */
	struct mutex mutex;		/* protection/blocking for read(), write() */
} scull_pipe;

static inline void print_circular_buffer_state(const char *operation) {
	struct scull_pipe *dev = &scull_pipe;

	dev_notice(scull_p_device, "%s() (%s): readp = %ld, writep = %ld, freespace = %ld", operation,
		   current->comm, dev->readp - dev->buffer, dev->writep - dev->buffer, dev->freespace);
}

/*
 *
 *                            FILE OPERATIONS
 *
 * The inode structure is used by the kernel internally to represent
 * files. Therefore, it is different from __the file structure__ that
 * represents an open file descriptor.
 *
 */

static int scull_p_open(struct inode *inode, struct file *file) {
	struct scull_pipe *dev;
	int ret = 0;

	dev = &scull_pipe;
	file->private_data = dev;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	if (!dev->buffer) {
		dev->buffer = devm_kmalloc(scull_p_device, SCULL_P_BUFFERSIZE, GFP_KERNEL);
		if (!dev->buffer) {
			ret = -ENOMEM;
			goto out;
		}

		dev->readp = dev->writep = dev->buffer;
		dev->end = dev->buffer + SCULL_P_BUFFERSIZE;
		dev->freespace = SCULL_P_BUFFERSIZE;
	}

out:
	mutex_unlock(&dev->mutex);
	return ret;
}

/*
 * Kernel keeps a reference count on the opened file descriptor.
 * Naturally fork() or dup() increases the refcount. Below release()
 * method is _only_ called with the refcount reaches 0.
 *
 * This relationship guarantees that the driver sees only one
 * release() for each call to open()
 */
static int scull_p_release(struct inode *inode, struct file *file) {
	return 0;
}

static ssize_t scull_p_read(struct file *file, char __user *buf, size_t count, loff_t *f_pos) {
	struct scull_pipe *dev;
	int ret = 0;

	dev = file->private_data;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	/*
	 * Nothing to read? Wait for writers!
	 *
	 * Careful! We do the wait_event_interruptible() check while mutex is
	 * _unlocked_. That's why we exit the loop only if the condition holds
	 * true while the lock is held.
	 */

	while (dev->freespace == SCULL_P_BUFFERSIZE) {

		mutex_unlock(&dev->mutex);

		if (file->f_flags & O_NONBLOCK)
			return -EAGAIN;

		dev_notice(scull_p_device, "read(): (%s) going to sleep!; freespace = %ld", current->comm, dev->freespace);

		if (wait_event_interruptible(dev->readersq, dev->freespace != SCULL_P_BUFFERSIZE))
			return -ERESTARTSYS;

		dev_notice(scull_p_device, "read(): (%s) Waking up!", current->comm);

		if (mutex_lock_interruptible(&dev->mutex))
			return -ERESTARTSYS;
	}

	/*
	 * Data to read. Go on!
	 */

	print_circular_buffer_state("read");

	if (dev->writep > dev->readp)
		count = min(count, (size_t)(dev->writep - dev->readp));	
	else if ((dev->writep < dev->readp) || (dev->writep == dev->readp && dev->freespace == 0))
		count = min(count, (size_t)(dev->end - dev->readp));

	ret = copy_to_user(buf, dev->readp, count);
	if (ret) {
		ret = -EFAULT;
		goto out;
	}

	/*
	 * Book-keeping and writers notification!
	 */

	dev->readp += count;
	dev->readp = (dev->readp >= dev->end) ? dev->buffer : dev->readp;

	BUG_ON((dev->freespace + count) > SCULL_P_BUFFERSIZE);
	BUG_ON((dev->freespace + count) < dev->freespace);	/* unsigned overflow */
	BUG_ON((dev->freespace + count) < count);		/* unsigned overflow */
	dev->freespace += count;

	wake_up_interruptible(&dev->writersq);

	print_circular_buffer_state("read");
	ret = count;
out:
	mutex_unlock(&dev->mutex);
	return ret;
}

static ssize_t scull_p_write(struct file *file, const char __user *buf, size_t count, loff_t *f_pos) {
	struct scull_pipe *dev;
	int ret;

	dev = file->private_data;

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	/*
	 * Buffer is full? Wait for readers!
	 */

	while (dev->freespace == 0) {

		mutex_unlock(&dev->mutex);

		dev_warn(scull_p_device, "write(): (%s) going to sleep! freespace = %ld", current->comm, dev->freespace);

		if (wait_event_interruptible(dev->writersq, dev->freespace != 0))
			return -ERESTARTSYS;

		dev_warn(scull_p_device, "write(): (%s) Waking up!", current->comm);

		if (mutex_lock_interruptible(&dev->mutex))
			return -ERESTARTSYS;
	}

	/*
	 * Freespace to write in. Go on!
	 */

	print_circular_buffer_state("write");

	if (dev->writep >= dev->readp)
		count = min(count, (size_t)(dev->end - dev->writep));
	else /* (dev->writep < dev->readp) */
		count = min(count, (size_t)(dev->readp - dev->writep));

	ret = copy_from_user(dev->writep, buf, count);
	if (ret) {
		ret = -EFAULT;
		goto out;
	}

	/*
	 * Book-keeping and readers notification!
	 */

	dev->writep += count;
	dev->writep = (dev->writep >= dev->end) ? dev->buffer : dev->writep;

	BUG_ON(count > dev->freespace);
	dev->freespace -= count;

	wake_up_interruptible(&dev->readersq);

	print_circular_buffer_state("write");
	ret = count;
out:
	mutex_unlock(&dev->mutex);
	return ret;
}

/*
 * Basic low-level select/poll/epoll implementation
 *
 * If this returns 0, the process caller will sleep until any event
 * happens in the wait queueus attached through poll_wait().
 *
 * When an event happens in any of the wait queues, this method get
 * called again.
 */
static unsigned int scull_p_poll(struct file *file, struct poll_table_struct *poll_table) {
	struct scull_pipe *dev;
	unsigned int ret_mask = 0;

	dev = file->private_data;

	mutex_lock_interruptible(&dev->mutex);

	poll_wait(file, &dev->readersq, poll_table);
	poll_wait(file, &dev->writersq, poll_table);

	if (dev->freespace < SCULL_P_BUFFERSIZE)
		ret_mask |= POLLIN | POLLRDNORM;
	if (dev->freespace > 0)
		ret_mask |= POLLOUT | POLLWRNORM;

	mutex_unlock(&dev->mutex);
	return ret_mask;
}

static struct file_operations scull_p_fops = {
	.owner = THIS_MODULE,
	.open = scull_p_open,
	.release = scull_p_release,
	.read = scull_p_read,
	.write = scull_p_write,
	.poll = scull_p_poll,
	.llseek = no_llseek,

	/* If your driver does not supply methods to handle the vector
	 * operations, readv and writev are implemented with multiple
	 * calls to your read and write methods. In many situations,
	 * however, greater efficiency is acheived by implementing
	 * readv and writev directly. */
};

static int scull_pipe_init(void) {

	init_waitqueue_head(&scull_pipe.readersq);
	init_waitqueue_head(&scull_pipe.writersq);
	mutex_init(&scull_pipe.mutex);

	return 0;
}

static int __init scull_p_module_init(void) {
	int r;

	scull_p_class = class_create(THIS_MODULE, SCULL_P_CLASS_GROUP);
	if (IS_ERR(scull_p_class)) {
		r = PTR_ERR(scull_p_class);
		goto out1;
	}

	scull_pipe_init();

	/* NOTE: As soon as register_chrdev() below succeeds, the device
	 * is "live" and its operations can be called by the kernel... */

	r = register_chrdev(0, SCULL_P_DEVICE_NAME, &scull_p_fops);
	if (r < 0) {
		printk(KERN_ERR "scull_pipe: Failed to register character device");
		goto out3;
	}
	scull_p_devid = MKDEV(r, 0);

	scull_p_device = device_create(scull_p_class, NULL, scull_p_devid, &scull_pipe, SCULL_P_DEVICE_NAME);
	if (IS_ERR(scull_p_device)) {
		r = PTR_ERR(scull_p_device);
		goto out2;
	}

	return 0;

out3:	device_destroy(scull_p_class, scull_p_devid);
out2:	class_destroy(scull_p_class);
out1:	return r;
}

static void __exit scull_p_module_exit(void) {
	device_destroy(scull_p_class, scull_p_devid);
	class_destroy(scull_p_class);
	unregister_chrdev(MAJOR(scull_p_devid), SCULL_P_DEVICE_NAME);
}

module_init(scull_p_module_init);
module_exit(scull_p_module_exit);
MODULE_LICENSE("GPL");
